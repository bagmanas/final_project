import numpy as np
import pickle
import requests
import redis
from flask import Flask, jsonify, request

app = Flask(__name__)

red = redis.Redis(host='65.21.55.167', port=6379, password='lolkek1488')

def get_cluster_name(query, db_connect):
    """
    Фукнция для получения ближйшего кластера
    db_connect - подключение к db Redis
    query - эмбединг запроса
    """
    min_cluster_dist = float('+inf')
    min_cluster_name = None
    for cluster_name in db_connect.lrange("cluster_names", 0, -1):
        cluser_center = pickle.loads(db_connect.hget(cluster_name,'center'))
        dist = np.linalg.norm(cluser_center-query)
        if dist < min_cluster_dist:
            min_cluster_dist = dist
            min_cluster_name = cluster_name
    return min_cluster_name

@app.route('/get_recomendation', methods=['POST'])
def get_recomendation():
    # Получаем эмбединг запроса
    r = requests.post('http://65.21.55.167:1478/get_embedding', json=request.json)
    query_emb = np.array(r.json()['embedding_list'])
    # Получаем ближйший кластер
    cluster_name = get_cluster_name(query_emb, red)
    host = red.hget(cluster_name, 'host').decode()
    port = red.hget(cluster_name, 'port').decode()

    # Отправляем запрос на кластер полученный выше
    d = r.json()
    r = requests.post(f'http://{host}:{port}/get_recomendation', json=d)
    return r.json()


if __name__ == '__main__':    
    app.run(port=1488, host='0.0.0.0')
