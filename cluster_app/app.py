import json
import sys
import signal
import redis
import pickle
import faiss
import numpy as np
from flask import Flask, jsonify, request

app = Flask(__name__)

host = sys.argv[1]
port = sys.argv[2]
cluster_id = int(sys.argv[3])

cluster_name = f'cluster_{cluster_id}_port_{port}'
red = redis.Redis(host='65.21.55.167', port=6379, password='lolkek1488')


with open(f'/data/{cluster_id}/question.pkl', 'rb') as f:
    question = pickle.load(f)
    
with open(f'/data/{cluster_id}/cluster_center.pkl', 'rb') as f:
    center = pickle.load(f)

index = faiss.read_index(f'/data/{cluster_id}/index.faiss')

def get_idx(embedding, top_k):
    D, I = index.search(embedding.reshape(1, -1), top_k) 
    return list(I[0])


@app.route('/get_recomendation', methods=['POST'])
def get_recomendation():
    embedding = np.array(request.json['embedding_list']).astype('float32')
    ans = []
    for idx in get_idx(embedding, 10):
        ans.append(question[idx])
    return jsonify(recomendation=ans)



def handler(sig, frame):
    # Разрегистрируем приложение
    red.delete(cluster_name)
    red.lrem('cluster_names', 0, cluster_name)
    sys.exit(0)

signal.signal(signal.SIGTERM, handler)
signal.signal(signal.SIGINT, handler)



if __name__ == '__main__':  
    # Регистрируем приложение
    red.lpush('cluster_names', cluster_name)
    red.hset(cluster_name, 'host', host)
    red.hset(cluster_name, 'port', port)
    red.hset(cluster_name, 'center', pickle.dumps(center, protocol=0))
    red.hset(cluster_name, 'version', 1)

    app.run(port=port, host='0.0.0.0')
