#!/bin/bash
# Скрипт для запуска / бесшовного обновления кластеров
# Получаем спиоск приложений с именем luster_*
runing_app=($(docker ps --filter "name=cluster_*" -q))

new_app=($(ls /var/lib/docker/volumes/cluster_data/_data/))
idx=0

# Последовательно тушим один старый кластер и запускаем новый
for var in $(ls /var/lib/docker/volumes/cluster_data/_data/)
do
echo " $var"
if [ ${runing_app[$idx]} ]
then
docker stop ${runing_app[$idx]}
docker rm ${runing_app[$idx]}
fi
port=$(python3 -c 'import socket; s=socket.socket(); s.bind(("", 0)); print(s.getsockname()[1]); s.close()')
docker run -d -p $port:$port --name cluster_$port -v cluster_data:/data cluster_app:v0.1 ./app.py 65.21.55.167 $port $var
((idx+=1))
done

# Если в старой версии кластеров больше, то тушим оставшиееся
while [ ${runing_app[$idx]} ]
do
echo "del" $idx
((idx+=1))
done
